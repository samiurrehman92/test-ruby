class ApplicationController < ActionController::API

	include ActionController::HttpAuthentication::Basic::ControllerMethods
	include ActionController::HttpAuthentication::Token::ControllerMethods

	before_action :http_basic_authenticate
	
	def http_basic_authenticate
	  authenticate_or_request_with_http_basic do |entered_username, entered_password|
		@logged_in_user = User.where(username: entered_username, password: entered_password).take
	  end
	end
end
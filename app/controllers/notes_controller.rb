class NotesController < ApplicationController
  before_action :set_note, only: [:show, :update, :destroy]

  # GET /notes
  def index
    @notes = Note.all

    render json: @notes
  end

  # POST /notes
  def create
  	if @logged_in_user.user_type === 'guest'
		render :json => { :errors => [{"title" => 'Guests are only allowed to view notes'}] }, :status => 422;
	end
	
    @note = Note.new(note_params)	
	@note.created_by = @logged_in_user.id
	
	
    if @note.save
      render json: @note, status: :created, location: @note
    else
      render json: @note.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /notes/1
  def update
  
  	if @logged_in_user.user_type === 'admin'
		if @note.update(note_params)
		  render json: @note
		else
		  render json: @note.errors, status: :unprocessable_entity
		end
	end
	if @logged_in_user.user_type === 'user'
		if @note.created_by == @logged_in_user.id
			if @note.update(note_params)
			  render json: @note
			else
			  render json: @note.errors, status: :unprocessable_entity
			end
		else
			render :json => { :errors => [{"title" => 'Guests are only allowed to view notes'}] }, :status => 422;
		end
	end
	if @logged_in_user.user_type === 'guest'
		render :json => { :errors => [{"title" => 'Guests are only allowed to view notes'}] }, :status => 422;
	end
	
    if @note.update(note_params)
      render json: @note
    else
      render json: @note.errors, status: :unprocessable_entity
    end
  end

  # DELETE /notes/1
  def destroy
	if @logged_in_user.user_type === 'admin'
		@note.destroy
	end
	if @logged_in_user.user_type === 'user'
		if @note.created_by == @logged_in_user.id
			@note.destroy
		else
			render :json => { :errors => [{"title" => 'Guests are only allowed to view notes'}] }, :status => 422;
		end
	end
	if @logged_in_user.user_type === 'guest'
		render :json => { :errors => [{"title" => 'Guests are only allowed to view notes'}] }, :status => 422;
	end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_note
      @note = Note.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def note_params
      params.fetch(:note, {}).permit(:text,:created_by)
    end
end

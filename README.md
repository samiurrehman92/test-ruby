# README

Dcumentation for Clark.de RAILS API test


Sample call for addition:
REQUEST TYPE: POST
URL: http://localhost:3000/notes
DATA: an array called ‘note’ with attribute ‘text’

Sample call for editing:
REQUEST TYPE: PUT
URL: http://localhost:3000/notes/:id
DATA: an array called ‘note’ with attribute ‘text’

Sample call for editing:
REQUEST TYPE: DELETE
URL: http://localhost:3000/notes/:id

P.S. In all cases required minimum role is necessary as mentioned in the test description.
require 'test_helper'

class NotesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @note = notes(:one)
  end
  
	load "#{Rails.root}/db/seeds.rb"
  
  test "should get all notes for user" do
    get notes_url, as: :json, headers: {"Authorization" => "Basic #{Base64.encode64('sami:test')}"}
    assert_response :success
  end

  test "should get all notes for guest" do
    get notes_url, as: :json, headers: {"Authorization" => "Basic #{Base64.encode64('ojaswi:test')}"}
    assert_response :success
  end

  test "should get all notes for admin" do
    get notes_url, as: :json, headers: {"Authorization" => "Basic #{Base64.encode64('jiayao:test')}"}
    assert_response :success
  end
  
  test "should create note for admin" do
    assert_difference('Note.count', +1) do
      post notes_url, params: { note: { :text : 'test note' } }, as: :json, headers: {"Authorization" => "Basic #{Base64.encode64('jiayao:test')}"}
    end
    assert_response 201
  end
  
  test "should create note for user" do
    assert_difference('Note.count', +1) do
      post notes_url, params: { note: { :text : 'test note' } }, as: :json, headers: {"Authorization" => "Basic #{Base64.encode64('sami:test')}"}
    end
    assert_response 201
  end  

  test "should not create note for guest" do
    assert_difference('Note.count') do
      post notes_url, params: { note: { :text : 'test note' } }, as: :json, headers: {"Authorization" => "Basic #{Base64.encode64('ojaswi:test')}"}
    end
    assert_response 200
  end

  test "should not destroy note for guest" do
    assert_difference('Note.count') do
      delete note_url(@note), as: :json, headers: {"Authorization" => "Basic #{Base64.encode64('ojaswi:test')}"}
    end
    assert_response 200
  end

  test "should destroy note for a user" do
    assert_difference('Note.count', -1) do
      delete note_url(@note), as: :json, headers: {"Authorization" => "Basic #{Base64.encode64('sami:test')}"}
    end
    assert_response 204
  end

  test "should destroy note for admin" do
    assert_difference('Note.count', -1) do
      delete note_url(@note), as: :json, headers: {"Authorization" => "Basic #{Base64.encode64('jiayao:test')}"}
    end
    assert_response 204
  end
  
end

class CreateUsers < ActiveRecord::Migration[5.1]
  def change
	  create_table "users", force: :cascade do |t|
		t.string "name"
		t.string "username"
		t.string "password"
		t.string "profession"
		t.string "user_type"
		t.datetime "created_at", null: false
		t.datetime "updated_at", null: false
    end
  end
end

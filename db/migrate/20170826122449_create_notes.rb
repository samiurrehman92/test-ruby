class CreateNotes < ActiveRecord::Migration[5.1]
  def change
	create_table "notes", force: :cascade do |t|
		t.string :text
		t.integer "created_by"
		t.datetime "created_at", null: false
		t.datetime "updated_at", null: false
    end
  end
end

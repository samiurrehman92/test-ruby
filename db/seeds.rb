Note.create!([
  {text: "text by jiayao cannot be edied by him", created_by: 1},
  {text: "text by sami", created_by: 2},
  {text: "note by sami with text can be edited by him", created_by: 2}
])
User.create!([
  {name: "Jiayao Peng", username: "jiayao", password: "test", profession: "Businesswoman", user_type: "admin"},
  {name: "Sami", username: "sami", password: "test", profession: "Software Engineer", user_type: "user"},
  {name: "Ojaswi Jhamb", username: "ojaswi", password: "test", profession: "Web Developer", user_type: "guest"}
])
